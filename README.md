Landon Chat Bot
===============

Description
-----------
Landon is a language-acquisition chatterbot modeled after programs like NIALL.

Landon attempts to learn the structure of the language spoken to it, as long as
that language uses spaces to separate words. It was originally written as an
exercise to learn machine learning and the Scheme programming language, having
been implemented with no reference material beyond a vague description of
NIALL. The concept was subsequently used as a practice exercise to learn Java
and C#.

Some time after Landon's first implementation was complete, it was discovered
that the technique is known as a first-order 
[Markov chain](https://en.wikipedia.org/wiki/Markov_chain).

Implementing Landon as an IRC bot has been considered, but so far work has not
been done in this direction in the interest of avoiding excessive spam.

Variants
--------
Source code is provided for the following variants:

- Scheme console
- MrScheme GUI
- Java console
- Java applet (obsolete)
- C# WinForms


Similar Projects
----------------
- [NIALL](http://gniall.sourceforge.net/download/niall.amos.txt) by Matthew Peck
- [gNiall](http://gniall.sourceforge.net/) on SourceForge


