import javax.swing.JApplet;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.Timer;

public class AppletMain extends JApplet {
    private void createGUI() {
        setContentPane(new LandonFrame());
    }

    public void init() {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() { createGUI(); }
            });
        } catch (Exception e) { 
            System.err.println("createGUI didn't successfully complete");
        }
    }

    public void start() {
    }

    public void stop() {
    }

    public String getAppletInfo() {
        return "Title: Landon\n"
               + "Author: Adam Higerd\n"
               + "A simple applet demo using Landon.";
    }

    public String[][] getParameterInfo() {
        String[][] info = { };
        return info;
    }
}
