import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class LandonFrame extends Box implements ActionListener, KeyListener {
    public JTextField textField;
    public JTextArea textArea;
    public JScrollPane scroll;
    Landon l = new Landon();

    public LandonFrame() {
        super(BoxLayout.Y_AXIS);
        textField = new JTextField("");
        textField.setMaximumSize(new Dimension(10000, 25));
        textField.addKeyListener(this);

        JButton button = new JButton("Send");
        button.addActionListener(this);

        scroll = new JScrollPane();

        textArea = new JTextArea();
        textArea.setEditable(false);

        scroll = new JScrollPane(textArea);

        Box box2 = new Box(BoxLayout.X_AXIS);
		this.add(scroll);
		this.add(box2);
		box2.add(textField);
		box2.add(button);

        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
    	l.addMessage(textField.getText());
    	textArea.setText(textArea.getText()+"<User> "+textField.getText()+"\n");
	   	textField.setText("");
    	textArea.setText(textArea.getText()+"<Landon> "+l.getMessage()+"\n");
    }


    public void keyTyped(KeyEvent e) {}
    public void keyReleased(KeyEvent e) {}
    public void keyPressed(KeyEvent e) {
    	int key = e.getKeyCode();
        if (key == KeyEvent.VK_ENTER) {
	    	l.addMessage(textField.getText());
	    	textArea.setText(textArea.getText()+"<User> "+textField.getText()+"\n");
		   	textField.setText("");
	    	textArea.setText(textArea.getText()+"<Landon> "+l.getMessage()+"\n");
        }
    }

    public void init() {}
}
