import java.io.*;
import java.util.*;

public class Landon {
    private class Entry {
        public int weight;
        public String word;

        public Entry(String newWord) {
            weight = 1;
            word = newWord;
        }

        public String toString() {
            return "(" + word + ":" + weight + ")";
        }
    }

    private class Node {
        public int totalWeight;
        public LinkedList<Entry> entries;

        public Node() {
            totalWeight = 0;
            entries = new LinkedList<Entry>();
        }

        public String toString() {
            return "[" + entries.toString() + "]";
        }

        public String selectWord() {
            int choice = (int)(Math.ceil(Math.random() * totalWeight));
            for(Entry entry : entries) {
                choice -= entry.weight;
                if(choice <= 0)
                    return entry.word;
            }
            return ""; // should only happen when empty
        }

        public void addWord(String word) {
            totalWeight++;
            String lower = word.toLowerCase();
            for(Entry entry : entries) {
                if(entry.word.toLowerCase() == lower) {
                    entry.weight++;
                    return;
                }
            }
            entries.add(new Entry(word));
        }
    }

    private class Dictionary {
        public HashMap<String, Node> entries;
        public Node root;

        public Dictionary() {
            entries = new HashMap<String, Node>();
            root = new Node();
        }

        public String toString() {
            return root.toString() + "\n-- " + entries.toString();
        }

        public void addWord(String start) {
            Node node = new Node();
            root.addWord(start);
        }

        public void addWord(String before, String after) {
            Node node = entries.get(before.toLowerCase());
            if(node == null) {
                node = new Node();
                entries.put(before.toLowerCase(), node);
            }
            node.addWord(after);
        }

        public String generateSentence() {
            String word = root.selectWord();
            String sentence = word;
            while(word.length() > 0) {
                Node node = entries.get(word.toLowerCase());
                word = node.selectWord();
                if(word.length() == 0) break;
                sentence = sentence + " " + word;
            }
            return sentence + ".";
        }
    }

    private Dictionary dict;

    public Landon() {
        dict = new Dictionary();
    }

    public Boolean addMessage(String message) {
        message = message.replaceAll("[^A-Za-z0-9- ]", "");
        if(message.matches(" *")) return false;
        String[] words = message.split(" ");
        String lastWord = "";
        for(String word : words) {
           if(lastWord.length() > 0) {
               dict.addWord(lastWord, word);
           } else {
               dict.addWord(word);
           }
           lastWord = word;
        }
        dict.addWord(lastWord, "");
        return true;
    }

    public String getMessage() {
        return dict.generateSentence();
    }

    public static void main(String[] args) throws Exception {
        Landon l = new Landon();
        InputStreamReader converter = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(converter);

        while(true) {
            System.out.print("<User> ");
            String line = in.readLine();
            if(line.length() > 0)
                l.addMessage(line);
            System.out.println("<Landon> " + l.getMessage());
        }
    }
}