namespace Landon
{
    partial class LandonUI
    {
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.inputBox = new System.Windows.Forms.TextBox();
            this.inputButton = new System.Windows.Forms.Button();
            this.historyBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // menuBar
            // 
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(292, 20);
            this.menuBar.TabIndex = 0;
            this.menuBar.Text = "menuStrip1";
            // 
            // inputBox
            // 
            this.inputBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.inputBox.Location = new System.Drawing.Point(3, 252);
            this.inputBox.Name = "inputBox";
            this.inputBox.Size = new System.Drawing.Size(210, 20);
            this.inputBox.TabIndex = 1;
            // 
            // inputButton
            // 
            this.inputButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.inputButton.Location = new System.Drawing.Point(217, 252);
            this.inputButton.Name = "inputButton";
            this.inputButton.Size = new System.Drawing.Size(72, 19);
            this.inputButton.TabIndex = 2;
            this.inputButton.Text = "Send";
            this.inputButton.UseVisualStyleBackColor = true;
            // 
            // historyBox
            // 
            this.historyBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.historyBox.Location = new System.Drawing.Point(0, 20);
            this.historyBox.Name = "historyBox";
            this.historyBox.ReadOnly = true;
            this.historyBox.Size = new System.Drawing.Size(292, 228);
            this.historyBox.TabIndex = 3;
            this.historyBox.Text = "";
            // 
            // LandonUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.historyBox);
            this.Controls.Add(this.inputButton);
            this.Controls.Add(this.inputBox);
            this.Controls.Add(this.menuBar);
            this.MainMenuStrip = this.menuBar;
            this.Name = "LandonUI";
            this.Text = "Landon";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.TextBox inputBox;
        private System.Windows.Forms.Button inputButton;
        private System.Windows.Forms.RichTextBox historyBox;
    }
}

