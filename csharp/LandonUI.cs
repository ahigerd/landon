using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Landon
{
	/// <summary>
	/// WinForms-based user interface class for Landon
	/// </summary>
	public partial class LandonUI : Form
	{
		private Landon core;
		private bool dirtyDict = false;
		private const string confirmClear = "The current dictionary has unsaved changes. Are you sure you want to clear it?";
		
		/// <summary>
		/// Initializes a new instance of the <see cref="Landon.LandonUI"/> class.
		/// </summary>
		/// <param name='core'>
		/// A reference to the Landon core object.
		/// </param>
		public LandonUI(Landon core)
		{
			this.core = core;
			InitializeComponent();
			this.inputButton.Click += this.HandleInput;
			this.inputBox.KeyPress += this.HandleInput;
			
			// Assembling menus here because mwf-designer lacks a proper menu editor implementation.
			
			ToolStripMenuItem fileMenu = new ToolStripMenuItem("&File");
			fileMenu.DropDownItems.Add(new ToolStripMenuItem("&New Chat", null, this.OnNewChat, Keys.Control | Keys.Shift | Keys.N));
			fileMenu.DropDownItems.Add(new ToolStripMenuItem("&Save Chat...", null, this.OnSaveChat, Keys.Control | Keys.Shift | Keys.S));
			fileMenu.DropDownItems.Add(new ToolStripSeparator());
			fileMenu.DropDownItems.Add(new ToolStripMenuItem("E&xit", null, this.OnFileExit));
			this.menuBar.Items.Add(fileMenu);
			
			ToolStripMenuItem dictMenu = new ToolStripMenuItem("&Dictionary");
			dictMenu.DropDownItems.Add(new ToolStripMenuItem("&New Dictionary", null, this.OnNewDict, Keys.Control | Keys.N));
			dictMenu.DropDownItems.Add(new ToolStripMenuItem("&Open Dictionary...", null, this.OnOpenDict, Keys.Control | Keys.O));
			dictMenu.DropDownItems.Add(new ToolStripMenuItem("&Merge Dictionary...", null, this.OnMergeDict, Keys.Control | Keys.M));
			dictMenu.DropDownItems.Add(new ToolStripMenuItem("&Save Dictionary...", null, this.OnSaveDict, Keys.Control | Keys.S));
			this.menuBar.Items.Add(dictMenu);
		}
		
		/// <summary>
		/// Starts a new chat by clearing the history.
		/// </summary>
		private void OnNewChat(object sender, System.EventArgs args)
		{
			this.historyBox.Clear();
			this.inputBox.Focus();
		}
		
		/// <summary>
		/// Saves the current chat log to a file.
		/// </summary>
		private void OnSaveChat(object sender, System.EventArgs args)
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Filter = "Text Files (*.txt)|*.txt";
			dlg.Title = "Save Chat";
			dlg.ShowHelp = true;
			dlg.ShowDialog();
			
			if(dlg.FileName != "") {
				using(StreamWriter file = new StreamWriter(dlg.FileName)) {
					file.Write(this.historyBox.Text);
				}
			}
			this.inputBox.Focus();
		}
		
		/// <summary>
		/// Closes the window, which will terminate the application.
		/// </summary>
		private void OnFileExit(object sender, System.EventArgs args)
		{
			Close();
		}
		
		/// <summary>
		/// Clears the dictionary after confirming with the user.
		/// </summary>
		private void OnNewDict(object sender, System.EventArgs args)
		{
			if(dirtyDict && MessageBox.Show(confirmClear, "Landon", MessageBoxButtons.YesNo) == DialogResult.No) {
				return;
			}
			core.ResetDictionary();
			this.inputBox.Focus();
			dirtyDict = false;
		}
		
		/// <summary>
		/// Ask the user for a dictionary to load, then load it.
		/// The original dictionary will be cleared.
		/// </summary>
		private void OnOpenDict(object sender, System.EventArgs args)
		{
			if(dirtyDict && MessageBox.Show(confirmClear, "Landon", MessageBoxButtons.YesNo) == DialogResult.No) {
				return;
			}

			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "Landon Dictionary Files (*.lan)|*.lan";
			dlg.Title = "Open Dictionary";
			dlg.ShowHelp = true;
			dlg.ShowDialog();
			if(dlg.FileName != "") {
				try {
					core.LoadDictionary(dlg.FileName);
					dirtyDict = false;
				} catch(Exception e) {
					MessageBox.Show("The specified file is not a valid Landon dictionary file.\n\n" + e.Message);
				}
			}
			this.inputBox.Focus();
		}
		
		/// <summary>
		/// Ask the user for a dictionary to load, then load it.
		/// The selected dictionary will be merged with the current dictionary.
		/// </summary>
		private void OnMergeDict(object sender, System.EventArgs args)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "Landon Dictionary Files (*.lan)|*.lan";
			dlg.Title = "Merge Dictionary";
			dlg.ShowHelp = true;
			dlg.ShowDialog();
			if(dlg.FileName != "") {
				try {
					core.MergeDictionary(dlg.FileName);
					dirtyDict = true;
				} catch(Exception e) {
					MessageBox.Show("The specified file is not a valid Landon dictionary file.\n\n" + e.Message);
				}
			}
			this.inputBox.Focus();
		}
		
		/// <summary>
		/// Prompts the user for a filename and saves the current dictionary to that file.
		/// </summary>
		private void OnSaveDict(object sender, System.EventArgs args)
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Filter = "Landon Dictionary Files (*.lan)|*.lan";
			dlg.Title = "Save Dictionary";
			dlg.ShowHelp = true;
			dlg.CreatePrompt = false;
			dlg.OverwritePrompt = true;
			dlg.ShowDialog();
			if(dlg.FileName != "") {
				try {
					core.SaveDictionary(dlg.FileName);
					dirtyDict = false;
				} catch(Exception e) {
					MessageBox.Show("An error occurred while saving.\n\n" + e.Message);
				}
			}
			this.inputBox.Focus();
		}
		
		/// <summary>
		/// Sends the content of the input box, if any, to the Landon core,
		/// requests a new statement from the core. and then adds both to
		/// the history buffer. Triggered by the Send button or by pressing
		/// Enter/Return.
		/// </summary>
		public void HandleInput(object sender, System.EventArgs args)
		{
			if(sender == this.inputBox) {
				KeyPressEventArgs keyArgs = (KeyPressEventArgs)args;
				if(keyArgs.KeyChar != (char)Keys.Return) return;
			}
			if(this.inputBox.Text.Length > 0) {
				AddMessage("User", this.inputBox.Text);
				this.core.ProcessLine(this.inputBox.Text);
				this.inputBox.Text = "";
				dirtyDict = true;
			}
			AddMessage("Landon", this.core.GenerateLine());
		}
		
		/// <summary>
		/// Adds a formatted message to the history buffer, rotating out
		/// old content if necessary, and scrolls to reveal the new content.
		/// </summary>
		/// <param name='sender'>
		/// The name of the person sending the message.
		/// </param>
		/// <param name='message'>
		/// The body of the message.
		/// </param>
		public void AddMessage(string sender, string message)
		{
			string newText = this.historyBox.Text + String.Format("<{0}> {1}\r\n", sender, message);
			while(newText.Length > 16000) {
				// WinForms multiline textbox caps at 32KB, and UCS-2 encoding is two bytes
				// per character, so truncate lines until we're under 16k characters.
				// Using 16000 instead of 16384 just to leave a little bit of just-in-case wiggle room.
				newText = newText.Substring(newText.IndexOf('\n') + 1);
			}
			this.historyBox.Text = newText;
			this.historyBox.Select(this.historyBox.TextLength, this.historyBox.TextLength);
			this.historyBox.ScrollToCaret();
		}
		
		/// <summary>
		/// The main entry point of the program.
		/// Initializes the Landon core and opens the UI.
		/// </summary>
        public static void Main()
        {
			Landon core = new Landon();
			Application.Run(new LandonUI(core));
        }		
	}
}
