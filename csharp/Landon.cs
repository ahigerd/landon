using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Landon
{
    /// <summary>
    /// The Landon dictionary core.
    /// </summary>
    public class Landon
    {
        public static Random RNG = new Random();
        private static readonly Char[] sentenceDelimiters = new Char[] { '.', '?', '!', ';' };
        private static readonly Char[] ignoredCharacters = new Char[] { ',', '(', ')', '"' }; 
        
        /// <summary>
        /// A structure defining a node first-order Markov chain, defining
        /// a set of transitions and relative probabilities for each.
        /// </summary>
        private class DictionaryWord
        {
            int totalWeight;
            Dictionary<string, int> weights;
            
            /// <summary>
            /// Initializes a new instance of the <see cref="Landon.Landon.DictionaryWord"/> class.
            /// </summary>
            public DictionaryWord()
            {
                totalWeight = 0;
                weights = new Dictionary<string, int>();
            }
            
            /// <summary>
            /// Adds all of the transitions from another DictionaryWord to this one,
            /// combining the weights.
            /// </summary>
            /// <param name='other'>
            /// The DictionaryWord to copy from
            /// </param>
            public void MergeWeights(DictionaryWord other)
            {
                foreach(KeyValuePair<string, int> freq in other.weights) {
                    this.AddWord(freq.Key, freq.Value);
                }    
            }
            
            /// <summary>
            /// Increases the weight of the specified word by 1, adding a new transition
            /// if necessary.
            /// </summary>
            /// <param name='word'>
            /// The next word to be added to the chain
            /// </param>
            public void AddWord(string word)
            {
                AddWord(word, 1);
            }
            
            /// <summary>
            /// Increases the weight of the specified word, adding a new transition if necessary.
            /// </summary>
            /// <param name='word'>
            /// The next word to be added to the chain
            /// </param>
            /// <param name='freq'>
            /// The amount of weight to add to the transition
            /// </param>
            public void AddWord(string word, int freq)
            {
                totalWeight += freq;
                foreach(char ch in ignoredCharacters) {
                    word = word.Replace(new string(ch, 1), "");
                }
                if(weights.ContainsKey(word))
                    weights[word] += freq;
                else
                    weights[word] = freq;
            }
            
            /// <summary>
            /// Randomly selects a word, weighted by the transition probabilities.
            /// </summary>
            /// <returns>
            /// A word, or "." to indicate the end of the chain.
            /// </returns>
            public string GetWord()
            {
                // If there are no transitions, return a sentinel.
                if(totalWeight == 0) return ".";
                // Pick a random number to select a transition.
                int target = RNG.Next(0, totalWeight);
                foreach(KeyValuePair<string, int> word in weights) {
                    target -= word.Value;
                    // When the appropriate weight is reached, return the word.
                    if(target < 0) return word.Key;
                }
                // Safety net. This should be impossible, but the compiler demands it.
                return ".";
            }
            
            /// <summary>
            /// Formats the current transitions in s-expression form for serialization.
            /// </summary>
            /// <returns>
            /// A <see cref="System.String"/> that represents the current <see cref="Landon.Landon.DictionaryWord"/>.
            /// </returns>
            public override string ToString()
            {
                // Approximating 14 bytes per entry will handle most English words.
                // In unusual cases it'll come up short but StringBuilder can handle that.
                StringBuilder repr = new StringBuilder(weights.Count * 14);    
                bool firstWord = true;
                foreach(KeyValuePair<string, int> word in weights) {
                    if(firstWord)
                        firstWord = false;
                    else
                        repr.Append(" ");
                    repr.AppendFormat("(\"{0}\" {1})", word.Key, word.Value);
                }
                return repr.ToString();
            }
        }

        private DictionaryWord root;
        private Dictionary<string, DictionaryWord> dictionary;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Landon.Landon"/> class.
        /// </summary>
        public Landon()
        {
            ResetDictionary();
        }
        
        /// <summary>
        /// Loads a dictionary from a file at the specified path, clearing the
        /// current dictionary.
        /// </summary>
        /// <param name='path'>
        /// The path to a Landon dictionary file
        /// </param>
        public void LoadDictionary(string path)
        {
            DictionaryWord newRoot;
            Dictionary<string, DictionaryWord> newDictionary;
            // ReadDictionary will throw on error; the caller is expected to handle it
            ReadDictionary(path, out newRoot, out newDictionary);
            
            root = newRoot;
            dictionary = newDictionary;
        }
        
        /// <summary>
        /// Loads a dictionary from a file at the specified path, merging it with
        /// the current dictionary.
        /// </summary>
        /// <param name='path'>
        /// The path to a Landon dictionary file
        /// </param>
        public void MergeDictionary(string path)
        {
            DictionaryWord newRoot;
            Dictionary<string, DictionaryWord> newDictionary;
            // ReadDictionary will throw on error; the caller is expected to handle it
            ReadDictionary(path, out newRoot, out newDictionary);
            
            root.MergeWeights(newRoot);
            foreach(KeyValuePair<string, DictionaryWord> word in newDictionary) {
                if(!dictionary.ContainsKey(word.Key))
                    dictionary[word.Key] = word.Value;
                else
                    dictionary[word.Key].MergeWeights(word.Value);
            }
        }
        
        /// <summary>
        /// Creates an exception indicating a parse failure while loading a dictionary file.
        /// </summary>
        /// <returns>
        /// A <see cref="System.FormatException">System.FormatException</a> object representing the error
        /// </returns>
        /// <param name='ch'>
        /// The character read from the file
        /// </param>
        /// <param name='pos'>
        /// The position of the character in the file
        /// </param>
        private Exception InvalidCharacterException(char ch, int pos)
        {
            return new System.FormatException(String.Format("unexpected character '{0}' at position {1}", ch, pos));
        }
        
        /// <summary>
        /// Reads a dictionary from a file.
        /// </summary>
        /// <param name='path'>
        /// The path to the dictionary file
        /// </param>
        /// <param name='root'>
        /// The root node of the loaded dictionary
        /// </param>
        /// <param name='dictionary'>
        /// The transition definitions of the loaded dictionary
        /// </param>
        /// <exception cref='System.FormatException'>
        /// Thrown when an error occurs during parsing
        /// </exception>
        private void ReadDictionary(string path, out DictionaryWord root, out Dictionary<string, DictionaryWord> dictionary)
        {
            // Initialize the new dictionary
            root = new DictionaryWord();
            dictionary = new Dictionary<string, DictionaryWord>();
            
            int state = 0;
            int pos = 0;
            StringBuilder token = new StringBuilder();
            string parentWord = ".", word = "";
            char delimiter = '"';
            
            // Open the file and loop through its characters
            using(StreamReader file = new StreamReader(path)) {
                while(file.Peek() >= 0) {
                    // Read the next character
                    pos++;
                    char ch = (char)file.Read();
                    // State machine parser
                    switch(state) {
                    case 0: // initial state
                        if(ch == '(') {
                            // dictionary start
                            state = 1;
                        }
                        // We ignore all invalid characters before the beginning
                        break;
                    case 1: // Inside top-level parens, find ( or )
                        if(ch == '(') {
                            // Word definition start
                            state = 2;
                        } else if(ch == ')') {
                            // End of dictionary
                            return;
                        } else if(!Char.IsWhiteSpace(ch)) {
                            throw InvalidCharacterException(ch, pos);
                        }
                        break;
                    case 2: // Inside word, find start of string
                        if(ch == '"' || ch == '\'') {
                            delimiter = ch;
                            state = 3;
                        } else if(!Char.IsWhiteSpace(ch)) {
                            throw InvalidCharacterException(ch, pos);
                        }
                        break;
                    case 3: // Inside string, consume to quote
                        if(ch == delimiter) {
                            parentWord = token.ToString().ToLower();
                            token.Length = 0; // token.Clear() requires .NET 4.0
                            state = 4;
                        } else {
                            token.Append(ch);
                        }
                        break;
                    case 4: // Whitespace before word frequency, consume to (
                        if(ch == '(') {
                            state = 5;
                        } else if(ch == ')') {
                            state = 1;
                        } else if(!Char.IsWhiteSpace(ch)) {
                            throw InvalidCharacterException(ch, pos);
                        }
                        break;
                    case 5: // Inside word frequency, find start of string
                        if(ch == '"' || ch == '\'') {
                            delimiter = ch;
                            state = 6;
                        } else if(!Char.IsWhiteSpace(ch)) {
                            throw InvalidCharacterException(ch, pos);
                        }
                        break;
                    case 6: // Inside string, consume to quote
                        if(ch == delimiter) {
                            word = token.ToString();
                            token.Length = 0; // token.Clear() requires .NET 4.0
                            state = 7;
                        } else {
                            token.Append(ch);
                        }
                        break;
                    case 7: // Whitespace, find digit
                        if(Char.IsDigit(ch)) {
                            state = 8;
                            goto case 8;
                        } else if(!Char.IsWhiteSpace(ch)) {
                            throw InvalidCharacterException(ch, pos);
                        } 
                        break;
                    case 8: // Number, consume digits, find whitespace or )
                        if(Char.IsDigit(ch)) {
                            token.Append(ch);
                        } else if(Char.IsWhiteSpace(ch) || ch == ')') {
                            int freq = Convert.ToInt32(token.ToString());
                            token.Length = 0; // token.Clear() requires .NET 4.0
                            // Add new transition to dictionary
                            if(parentWord == ".") {
                                root.AddWord(word, freq);
                            } else {
                                if(!dictionary.ContainsKey(parentWord))
                                    dictionary[parentWord] = new DictionaryWord();
                                dictionary[parentWord].AddWord(word, freq);
                            }
                            state = 9;
                            goto case 9;
                        } else {
                            throw InvalidCharacterException(ch, pos);
                        }
                        break;
                    case 9: // post-number whitespace, find )
                        if(ch == ')') {
                            state = 4; // look for next word definition
                        } else if(!Char.IsWhiteSpace(ch)) {
                            throw InvalidCharacterException(ch, pos);
                        }
                        break;
                    }
                }
            }
            // If we didn't hit the "return;" in case 1, the dictionary is incomplete.
            throw new System.FormatException(String.Format("unexpected end of file at position {0}", pos));
        }
        
        /// <summary>
        /// Saves the current dictionary to a file in s-expression format.
        /// </summary>
        /// <param name='path'>
        /// The path to the dictionary file
        /// </param>
        public void SaveDictionary(string path)
        {
            // File format:
            // file        := IGNORE* + "(" + WS* + node + ")" + IGNORE*
            // node        := "(" + WS* + quoted-word + WS* + transitions + ")" + WS*
            // transitions := "(" + WS* + transition* + ")" + WS*
            // transition  := "(" + WS* + quoted-word + WS* + UInt32 + WS* + ")" + WS*
            using(StreamWriter file = new StreamWriter(path)) {
                file.Write("(");
                file.Write(String.Format("(\".\" {0})", root.ToString()));
                foreach(KeyValuePair<string, DictionaryWord> word in dictionary) {
                    file.Write(String.Format(" (\"{0}\" {1})", word.Key, word.Value.ToString()));
                }
                file.Write(")");
            }
        }
        
        /// <summary>
        /// Resets the dictionary.
        /// </summary>
        public void ResetDictionary()
        {
            root = new DictionaryWord();
            dictionary = new Dictionary<string, DictionaryWord>();
        }
        
        /// <summary>
        /// Processes a line of text provided by the user, generating new transitions
        /// in the hidden Markov model.
        /// </summary>
        /// <param name='content'>
        /// The line of text
        /// </param>
        public void ProcessLine(string content)
        {
            // Remove ignored characters from the input
            foreach(char ch in ignoredCharacters) {
                content = content.Replace(new string(ch, 1), "");
            }
            // Split the input into sentences
            string[] sentences = content.Split(sentenceDelimiters);
            foreach(string sentence in sentences) {
                // Split the sentence into words
                string[] words = sentence.Split(new Char[] { ' ' });
                string lastWord = null;
                foreach(string word in words) {
                    // Ignore contiguous whitespace
                    if(word == "") continue;
                    // Add the word to the current node in the hidden Markov model
                    if(lastWord == null) {
                        root.AddWord(word);
                    } else {
                        dictionary[lastWord].AddWord(word);
                    }
                    // Move to the next node, creating a new one if necessary
                    lastWord = word.ToLower();
                    if(!dictionary.ContainsKey(lastWord)) {
                        dictionary[lastWord] = new DictionaryWord();
                    }
                }
                // If any transitions were added to the model, add a sentinel
                if(lastWord != null)
                    dictionary[lastWord].AddWord(".");
            }
        }
        
        /// <summary>
        /// Generates a line of text based on the hidden Markov model.
        /// </summary>
        /// <returns>
        /// The line of text
        /// </returns>
        public string GenerateLine()
        {
            string line = null;
            // Get a starting state for the Markov chain
            string word = root.GetWord();
            while(word != ".") {
                // Add the word to the output
                if(line == null)
                    line = word;
                else
                    line = line + " " + word;
                // Move to the next state in the Markov chain
                word = dictionary[word.ToLower()].GetWord();
            }
            return line + ".";
        }
    }
}
